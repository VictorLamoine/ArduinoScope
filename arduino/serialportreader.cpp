#include "serialportreader.h"

#include <QCoreApplication>
#include <QDebug>

QT_USE_NAMESPACE

SerialPortReader::SerialPortReader(QSerialPort *serial_port,
                                   QObject *parent) :
        QObject(parent),
        serial_port_(serial_port)
{
  connect(serial_port, SIGNAL(readyRead()), SLOT(readData()));
  connect(serial_port, SIGNAL(error(QSerialPort::SerialPortError)), SLOT(handleError(QSerialPort::SerialPortError)));
}

SerialPortReader::~SerialPortReader()
{
}

void SerialPortReader::readData()
{
  data_read_.append(serial_port_->readAll());
  if (!data_read_.contains('\n'))
    return; // Wait for a complete line

  // Keep only last message if there are multiple ones
  const int last_lr(data_read_.lastIndexOf('\n'));

  // Remove extra characters after last \n
  if (last_lr != data_read_.size() - 1)
    data_read_.truncate(last_lr + 1);

  // While first \n is not last
  int index(data_read_.indexOf('\n'));
  while (index != data_read_.size() - 1)
  {
    // Remove characters on the left
    data_read_ = data_read_.right(data_read_.size() - index - 1);

    // Search for first \n, don't stop until it's the last character
    index = data_read_.indexOf('\n');
  }

  QDateTime now(QDateTime::currentDateTime()); // Not good because it's the stamp of the last byte message!
  emit newLineFetched(data_read_, now);
  data_read_.clear();
}

void SerialPortReader::handleError(QSerialPort::SerialPortError errror)
{
  if (errror == QSerialPort::ReadError)
  {
    qDebug() << "An I/O error occurred while reading the data from port" << serial_port_->portName() << ", error:"
        << serial_port_->errorString();
    while (1)
    {
    } // FIXME: Emit signal to exit
  }
}
