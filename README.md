 [<img src="/icons/arduinoscope.png" align="center" height="100">](.) ArduinoScope
===
Record, display and export multiple sensors data coming from your Arduino.

![MainWindow](/documentation/MainWindow.png)

# Dependencies
- [Qt 5.5](http://www.qt.io/download-open-source/) (with `QPrintSupport` and `QSerialPort` support)

Install Qt on Ubuntu:
```bash
sudo apt-get install qttools5-dev qt5-default qtmultimedia5-dev qtmultimedia5-examples libqt5serialport5-dev
```

# How to build
Clone the project or [download](https://gitlab.com/VictorLamoine/ArduinoScope/repository/archive.zip?ref=master) the latest archive.

Build using `cmake`:
```bash
mkdir build
cd build
cmake ..
make -j2
```

# Install
```bash
sudo make -j2 install
```

Launch manually for the first time:
```
gtk-launch arduinoscope.desktop
```

A launcher is now created and available through the application menu.

# Documentation
[User guide](/documentation/README.md)

# Arduino code
The [Arduino](/documentation/arduino) directory contains example Arduino codes to be able to use the application.

# Licence
See [LICENSE.txt](/LICENSE). This project uses `QCustomPlot`, please read [the library licence](/qcustomplot/GPL.txt).
